package fr.upem.partiel.part2.parser

import java.time.Year
import java.util.Calendar

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie._
import play.api.libs.functional.syntax._
import play.api.libs.json._

object Parser {

  // TODO
  def toDirector: String => Option[Director] = (fn : String) => ??? /*new Director {
    override def fullname: String = fn

    override def fn: String = fn.split(" ")(0)

    override def ln: String = fn.split(" ")(1)
  }*/


  def toName: String => Title = (title: String) => new Title {
    override def name: String = title
  }


  def toCountry: String => Option[Country] = (s : String) => s match {
    case "FR" => Some(Country.France)
    case "US" => Some(Country.UnitedStates)
    case "IT" => Some(Country.Italy)
    case "UK" => Some(Country.England)
    case "GE" => Some(Country.Germany)

  }

  def toYear: String => Option[Year] = (s : String) => Some(Year.of(s.toInt))



  def toViews: BigDecimal => Option[Views] =  /*(nb : Long) =>*/ ??? /* Some(new Views {
    override def views: Long = nb
  })*/

  implicit val directorReads = Reads[Director] {
    case JsString(value) => toDirector(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Director"))
    case _ => JsError("Not a valid type for Director")
  }

  implicit val nameReads = Reads[Title] {
    case JsString(value) => JsSuccess(toName(value))
    case _ => JsError("Not a valid type for Name")
  }

  implicit val countryReads = Reads[Country] {
    case JsString(value) => toCountry(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Country"))
    case _ => JsError("Not a valid type for Country")
  }

  implicit val yearReads = Reads[Year] {
    case JsString(value) => toYear(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Year"))
    case _ => JsError("Not a valid type for Year")
  }

  implicit val viewsReads = Reads[Views] {
    case JsNumber(value) => toViews(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Views"))
    case _ => JsError("Not a valid type for Views")
  }

  /*implicit val movieReads: Reads[Movie] = (
    (__ \ "title").read[Title] and
      (__ \ "director").read[Director] and
      (__ \ "year").read[Year] and
      (__ \ "views").read[Views] and
      (__ \ "country").read[Country]
    ) (Movie.apply _)
*/
}
