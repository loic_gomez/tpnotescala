package fr.upem.partiel.part2.functions

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.Director


object Functions {

  // TODO
  lazy val getDirectorNames: List[Movie] => List[Object] = (l : List[Movie]) => l match {
    case Nil => Nil
    case ::(head, tl) => head.asInstanceOf[Movie]::getDirectorNames(tl)
  }

  // TODO
  lazy val viewMoreThan: Long => List[Movie] => List[Movie] = (nb:Long) => (l : List[Movie]) => ??? /*l.map((m: Movie) => m.views>nb)*/

  // TODO
  lazy val byDirector: List[Movie] => Map[Director, List[Movie]] = (l : List[Movie]) => ???

}
