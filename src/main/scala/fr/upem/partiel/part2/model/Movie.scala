package fr.upem.partiel.part2.model

// TODO You have to create all the classes you need for the exam
// TODO Don't forget to read the existing code and the unit tests to get some clues !

// TODO Create this model
trait Movie

object Movie {

  def apply: Movie = new Movie {
    def title: Title = title

    def director: Director = director

    def views: Views = views

    def year: Year = year

    def country: Country = country
  }

  /*abstract class Movie {
    def title: Title

    def director: Director

    def views: Views

    def year: Year

    def country: Country
  }*/


  trait Title {
    def name: String
  }

  trait Director{
    def fullname: String
    def fn: String
    def ln: String
  }


  trait Year{
    def year: Int
  }

  trait Views{
    def views: Long
  }

  trait Country

  object Country {

    final case object France extends Country

    final case object England extends Country

    final case object Italy extends Country

    final case object Germany extends Country

    final case object UnitedStates extends Country

  }


  def movie(title: Title, director: Director, year: Year, views: Views, country: Country): Movie = Movie.apply


  def title(s: String): Title = new Title {
    override def name: String = s
  }

  def director(fn: String, ln: String): Director = new Director {
    override def fullname: String = fn + " " + ln

    override def fn: String = fn

    override def ln: String = ln
  }

  def year(value: Int): Year = new Year {
    override def year: Int = value
  }

  def views(value: Long): Views = new Views {
    override def views: Long = value
  }

}